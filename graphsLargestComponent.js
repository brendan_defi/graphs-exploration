"use strict"

const largestComponent = (graph) => {
    let biggestComponent = 0;
    const seen = new Set()

    for (const node in graph) {
        const componentSize = exploreAndCount(graph, node, seen)
        biggestComponent = Math.max(componentSize, biggestComponent);
    }


    return biggestComponent;
};

const exploreAndCount = (graph, root, seen) => {
    if (seen.has(String(root))) return 0;
    seen.add(String(root));

    let componentSize = 1;
    for (const neighbor of graph[root]) {
        componentSize += exploreAndCount(graph, neighbor, seen);
    }
    
    return componentSize;
}


const g = {
    0: [8, 1, 5],
    1: [0],
    5: [0, 8],
    8: [0, 5],
    2: [3, 4],
    3: [2, 4],
    4: [3, 2],
}

console.log(largestComponent(g));
// should be 4

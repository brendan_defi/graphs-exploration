"use strict"

const minIslandSize = (binaryMatrix) => {
    let smallestIslandSize = Infinity;
    const considered = new Set();

    for (let row = 0; row < binaryMatrix.length; row++) {
        for (let col = 0; col < binaryMatrix[0].length; col++) {
            const current = binaryMatrix[row][col];
            if (current === 1 && !considered.has(`${row},${col}`)) {
                const currentIslandSize = exploreAndCount(binaryMatrix, row, col, considered);
                smallestIslandSize = Math.min(smallestIslandSize, currentIslandSize);
            }
        }
    }

    if (smallestIslandSize === Infinity) {
        return 0;
    }
    return smallestIslandSize;
};


const exploreAndCount = (graph, row, col, consideredSet) => {
    if (
        row < 0 ||
        row >= graph.length ||
        col < 0 ||
        col >= graph[0].length ||
        consideredSet.has(`${row},${col}`) ||
        graph[row][col] === 0
    ) return 0;

    consideredSet.add(`${row},${col}`)

    let islandSize = 1;
    islandSize += exploreAndCount(graph, row - 1, col, consideredSet);
    islandSize += exploreAndCount(graph, row + 1, col, consideredSet);
    islandSize += exploreAndCount(graph, row, col - 1, consideredSet);
    islandSize += exploreAndCount(graph, row, col + 1, consideredSet);

    return islandSize;
};


const i4 = [
    [1, 1, 1, 0, 1, 0],
    [0, 0, 0, 0, 1, 1],
    [0, 0, 0, 0, 1, 0],
    [1, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1],
    [0, 1, 1, 1, 0, 1],
]
console.log(minIslandSize(i4));
// 2

const i1 = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
]
console.log(minIslandSize(i1));
// 

const i2 = [
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
]
console.log(minIslandSize(i2));
// 36

const i3 = [
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
]
console.log(minIslandSize(i3));
// 1

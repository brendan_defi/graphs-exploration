"use strict"

const maxIslandValue = (binaryMatrix) => {
    let maxValue = 0;
    const considered = new Set();

    for (let row = 0; row < binaryMatrix.length; row++) {
        for (let col = 0; col < binaryMatrix[0].length; col++) {
            const current = binaryMatrix[row][col];

            if (
                !considered.has(`${row},${col}`) &&
                current > 0
            ) {
                const islandValue = exploreAndValue(binaryMatrix, row, col, considered);
                maxValue = Math.max(maxValue, islandValue);
            }
        }
    }

    return maxValue;
};


const exploreAndValue = (grid, r, c, consideredSet) => {
    if (
        r < 0 ||
        r >= grid.length ||
        c < 0 ||
        c >= grid[0].length ||
        consideredSet.has(`${r},${c}`) ||
        grid[r][c] === 0
    ) return 0;

    let islandValue = grid[r][c];
    consideredSet.add(`${r},${c}`);

    islandValue += exploreAndValue(grid, r - 1, c, consideredSet);
    islandValue += exploreAndValue(grid, r + 1, c, consideredSet);
    islandValue += exploreAndValue(grid, r, c - 1, consideredSet);
    islandValue += exploreAndValue(grid, r, c + 1, consideredSet);

    return islandValue;
};




const i1 = [
    [2, 3, 4, 0, 1, 0],
    [0, 0, 0, 0, 7, 1],
    [0, 0, 0, 0, 1, 0],
    [2, 0, 0, 0, 0, 3],
    [9, 0, 9, 0, 1, 2],
    [0, 1, 5, 1, 0, 9],
]
console.log(maxIslandValue(i1));
// 16

const i2 = [
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
]
console.log(maxIslandValue(i2));
// 36

const i3 = [
    [1, 0, 1, 0, 1, 0],
    [0, 10, 0, 1, 0, 1],
    [1, 0, 100, 0, 1, 0],
    [0, 1, 0, 50, 0, 1],
    [1, 0, 69420, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
]
console.log(maxIslandValue(i3));
// 69420

const i4 = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
]
console.log(maxIslandValue(i4));
// 0

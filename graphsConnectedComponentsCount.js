"use strict"

const connectedComponentsCount = (graph) => {
    let ccCount = 0;
    const visited = new Set();

    for (const graphNode in graph) {
        ccCount += bfs(graph, graphNode, visited);
    }

    return ccCount;
};


const bfs = (graph, root, visited) => {
    if (visited.has(parseInt(root))) return 0;
    const queue = [root];

    while (queue.length > 0) {
        const current = queue.shift();
        visited.add(parseInt(current));

        const neighbors = graph[root];
        for (const neighbor of neighbors) {
            if (!visited.has(parseInt(neighbor))) {
                queue.push(neighbor);
            }
        }
    }
    return 1;
}


const g = {
    0: [8, 1, 5],
    1: [0],
    5: [0, 8],
    8: [0, 5],
    2: [3, 4],
    3: [2, 4],
    4: [3, 2],
}

console.log(connectedComponentsCount(g));
// should be 2

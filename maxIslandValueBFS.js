"use strict"

const maxIslandValueBFS = (binaryArray) => {
    let maxValue = 0;
    const considered = new Set();

    for (let row = 0; row < binaryArray.length; row++) {
        for (let col = 0; col < binaryArray[0].length; col++) {
            const current = binaryArray[row][col];

            if (
                !considered.has(`${row},${col}`) &&
                current > 0
            ) {
                const islandValue = exploreAndValue(binaryArray, row, col, considered);
                maxValue = Math.max(maxValue, islandValue);
            }
        }
    }

    return maxValue;
};


const exploreAndValue = (grid, r, c, consideredSet) => {
    let islandValue = 0;
    const queue = [[r, c]];
    consideredSet.add(`${r},${c}`)

    while (queue.length > 0) {
        const current = queue.shift();
        const [currentRow, currentCol] = current;
        islandValue += grid[currentRow][currentCol]

        const neighbors = [
            [currentRow - 1, currentCol],
            [currentRow + 1, currentCol],
            [currentRow, currentCol - 1],
            [currentRow, currentCol + 1],
        ]

        for (const neighbor of neighbors) {
            const [neighborRow, neighborCol] = neighbor;

            if (
                neighborRow >= 0 &&
                neighborRow < grid.length &&
                neighborCol >= 0 &&
                neighborCol < grid[0].length &&
                !consideredSet.has(`${neighborRow},${neighborCol}`) &&
                grid[neighborRow][neighborCol] !== 0
            ) {
                queue.push(neighbor);
                consideredSet.add(`${neighborRow},${neighborCol}`);
            }
        }
    }

    return islandValue;
};


const i1 = [
    [2, 3, 4, 0, 1, 0],
    [0, 0, 0, 0, 7, 1],
    [0, 0, 0, 0, 1, 0],
    [2, 0, 0, 0, 0, 3],
    [9, 0, 9, 0, 1, 2],
    [0, 1, 5, 1, 0, 9],
]
console.log(maxIslandValueBFS(i1));
// 16

const i2 = [
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
]
console.log(maxIslandValueBFS(i2));
// 36

const i3 = [
    [1, 0, 1, 0, 1, 0],
    [0, 10, 0, 1, 0, 1],
    [1, 0, 100, 0, 1, 0],
    [0, 1, 0, 50, 0, 1],
    [1, 0, 69420, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
]
console.log(maxIslandValueBFS(i3));
// 69420

const i4 = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
]
console.log(maxIslandValueBFS(i4));
// 0

"use strict"

const findOrder = (numCourses, prerequisites) => {
    const courseOrder = [];
    const [courseMap, prereqCounts]  = prereqListToCourseMap(numCourses, prerequisites);
    // const prereqCounts = calcPrereqs(courseMap);

    const queue = [];
    for (let i = 0; i < prereqCounts.length; i++) {
        const prereqCount = prereqCounts[i];
        if (prereqCount === 0) {
            queue.push(i);
        }
    }

    while (queue.length > 0) {
        const current = queue.shift();
        courseOrder.push(current);
        const unlockedCourses = courseMap[current];
        for (const unlockedCourse of unlockedCourses) {
            prereqCounts[unlockedCourse]--;
            if (prereqCounts[unlockedCourse] === 0) {
                queue.push(unlockedCourse);
            }
        }
    }

    if (courseOrder.length !== numCourses) {
        return [];
    }
    return courseOrder;
};


const prereqListToCourseMap = (numNodes, edges) => {
    const courseMap = {};
    const prereqCounts = new Array(numNodes).fill(0);

    for (let i = 0; i < numNodes; i++) {
        courseMap[i] = [];
    }

    for (const [course, prereq] of edges) {
        courseMap[prereq].push(course);
        prereqCounts[course]++;
    }

    return [courseMap, prereqCounts];
};


// const calcPrereqs = (map) => {
//     const prereqCounts = new Array(Object.keys(map).length).fill(0);

//     for (const course in map) {
//         prereqCounts[course] = map[course].length;
//     }

//     return prereqCounts
// };


// const bfs = (root, graph, consideredSet) => {
//     const route = [];
//     const queue = [root];

//     while (queue.length > 0) {
//         const currentNode = queue.shift();
//         consideredSet.add(currentNode);
//         if (route.indexOf(currentNode) === -1) {
//             route.push(currentNode);
//         }

//         const neighbors = graph[currentNode];
//         if (neighbors) {
//             for (const neighbor of neighbors) {
//                 if (!consideredSet.has(neighbor)) {
//                     queue.push(neighbor);
//                 }
//             }
//         }
//     }

//     return route;
// };



let x;
let y;

x = 2;
y = [[1,0]];
console.log(findOrder(x, y));
// [0, 1]

x = 2;
y = [[0,1]];
console.log(findOrder(x, y));
// [1, 0]

x = 4;
y = [[1,0],[2,0],[3,1],[3,2]];
console.log(findOrder(x, y));
// [0, 1, 2, 3] or [0, 2, 1, 3]

x = 1;
y = [];
console.log(findOrder(x, y));
// [0]

x = 4;
y = [[1,0],[1,2],[2,3],[3,1]];
console.log(findOrder(x, y));
// []


// for (let firstCourse = 0; firstCourse < numCourses; firstCourse++) {
//     const considered = new Set();
//     let coursesCompleted = bfs(firstCourse, prereqMap, considered);
//     if (coursesCompleted.length === numCourses) {
//         return coursesCompleted;
//     }
// }
// return [];

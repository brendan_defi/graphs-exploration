"use strict"

const g = {
    a: ['b', 'c'],
    b: ['d'],
    c: ['e'],
    d: ['f'],
    e: [],
    f: [],
}


const iterativeDepthFirstPrint = (graph, startingNode) => {
    const stack = [startingNode];

    while (stack.length > 0) {
        const currentNode = stack.pop();
        console.log(currentNode);

        const neighbors = graph[currentNode];
        for (const neighbor of neighbors) {
            stack.push(neighbor);
        }
    }
}

const recursiveDepthFirstPrint = (graph, startingNode) => {
    console.log(startingNode);
    const neighbors = graph[startingNode];

    if (neighbors.length === 0) return;

    for (const neighbor of neighbors) {
        recursiveDepthFirstPrint(graph, neighbor);
    }
}

// console.log(iterativeDepthFirstPrint(g, "a"));
// console.log(recursiveDepthFirstPrint(g, "a"));

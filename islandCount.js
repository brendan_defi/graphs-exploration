"use strict"

// assumption: binaryMatrix is a rectangular grid (ie all rows are of the same length)
// restriction: not allowed to mutate the input array

const islandCount = (binaryMatrix) => {
    let count = 0;
    const considered = new Set();

    for (let row = 0; row < binaryMatrix.length; row++) {
        for (let col = 0; col < binaryMatrix[0].length; col++) {
            const current = binaryMatrix[row][col];
            if (current === 1 && !considered.has(`${row},${col}`)) {
                count ++;
                explore(binaryMatrix, row, col, considered)
            }
        }
    }

    return count;
};


const explore = (graph, row, col, consideredSet) => {
    if (
        row < 0 ||
        row >= graph.length ||
        col < 0 ||
        col >= graph[0].length ||
        consideredSet.has(`${row},${col}`) ||
        graph[row][col] === 0
    ) return;

    consideredSet.add(`${row},${col}`)

    explore(graph, row - 1, col, consideredSet);
    explore(graph, row + 1, col, consideredSet);
    explore(graph, row, col - 1, consideredSet);
    explore(graph, row, col + 1, consideredSet);
};


const i4 = [
    [1, 1, 1, 0, 1, 0],
    [0, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 1, 0],
    [1, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 1],
    [0, 1, 1, 1, 0, 1],
]
console.log(islandCount(i4));
// 7

const i1 = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
]
console.log(islandCount(i1));
// 0

const i2 = [
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
]
console.log(islandCount(i2));
// 1

const i3 = [
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
]
console.log(islandCount(i3));
// 18

"use strict"

const numberIslands = (grid) => {
    let islandCount = 0;

    for (let row = 0; row < grid.length; row++) {
        for (let col = 0; col < grid[0].length; col++) {
            const currentNode = grid[row][col];

            if (currentNode === "1" ) {
                islandCount++;
                // DFT(grid, row, col);
                BFT(grid, row, col);
            }
        }
    }
    return islandCount;
};


// const DFT = (grid, row, col) => {
//     if (
//         row < 0 ||
//         row >= grid.length ||
//         col < 0 ||
//         col >= grid[0].length ||
//         grid[row][col] === "#" ||
//         grid[row][col] === '0'
//     ) return;

//     grid[row][col] = "#";

//     DFT(grid, row - 1, col);
//     DFT(grid, row + 1, col);
//     DFT(grid, row, col - 1);
//     DFT(grid, row, col + 1);
// }


const BFT = (grid, row, col) => {
    const queue = [[row, col]]

    while (queue.length > 0) {
        const currentCoordinate = queue.shift();
        const currentRow = currentCoordinate[0];
        const currentCol = currentCoordinate[1];

        if (grid[currentRow][currentCol] === "#") continue;

        grid[currentRow][currentCol] = "#";
        const neighborCoordinates = [
            [currentRow - 1, currentCol],
            [currentRow + 1, currentCol],
            [currentRow, currentCol - 1],
            [currentRow, currentCol + 1]
        ]
        for (const neighborCoordinate of neighborCoordinates) {
            const neighborRow = neighborCoordinate[0];
            const neighborCol = neighborCoordinate[1];

            if (
                neighborRow >= 0 &&
                neighborRow < grid.length &&
                neighborCol >= 0 &&
                neighborCol < grid[0].length &&
                grid[neighborRow][neighborCol] !== "#" &&
                grid[neighborRow][neighborCol] !== '0'
            ) {
                queue.push(neighborCoordinate);
            }
        }
    }
};



let grid1 = [
    ["1","1","1","1","0"],
    ["1","1","0","1","0"],
    ["1","1","0","0","0"],
    ["0","0","0","0","0"]
]
console.log(numberIslands(grid1))
// should be 1

let grid2 = [
    ["1","1","0","0","0"],
    ["1","1","0","0","0"],
    ["0","0","1","0","0"],
    ["0","0","0","1","1"]
  ]
console.log(numberIslands(grid2))
// should be 2

let grid3 = [
    ["0","0","0","0","0"],
    ["0","0","0","0","0"],
    ["0","0","0","0","0"],
    ["0","0","0","0","0"]
  ]
console.log(numberIslands(grid3))
// should be 0

let grid4 = [
    []
]
console.log(numberIslands(grid4))
// should be 0

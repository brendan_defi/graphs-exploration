"use strict"

const minIslandSize = (binaryMatrix) => {
    let smallestIslandSize = Infinity;
    const considered = new Set();

    for (let row = 0; row < binaryMatrix.length; row++) {
        for (let col = 0; col < binaryMatrix[0].length; col++) {
            const current = binaryMatrix[row][col];
            if (current === 1 && !considered.has(`${row},${col}`)) {
                const currentIslandSize = exploreAndCount(binaryMatrix, row, col, considered);
                smallestIslandSize = Math.min(smallestIslandSize, currentIslandSize);
            }
        }
    }

    if (smallestIslandSize === Infinity) {
        return 0;
    }
    return smallestIslandSize;
};


const exploreAndCount = (graph, row, col, consideredSet) => {
    if (
        row < 0 ||
        row >= graph.length ||
        col < 0 ||
        col >= graph[0].length ||
        consideredSet.has(`${row},${col}`) ||
        graph[row][col] === 0
    ) return 0;

    let islandSize = 0;

    const queue = [[row, col]];
    consideredSet.add(`${row},${col}`)

    while (queue.length > 0) {
        const current = queue.shift();
        const [currentRow, currentCol] = current;
        islandSize++;

        const neighbors = [
            [currentRow - 1, currentCol],
            [currentRow + 1, currentCol],
            [currentRow, currentCol - 1],
            [currentRow, currentCol + 1]
        ]

        for (const neighbor of neighbors) {
            const [neighborRow, neighborCol] = neighbor;

            if (
                neighborRow >= 0 &&
                neighborRow < graph.length &&
                neighborCol >= 0 &&
                neighborCol < graph[0].length &&
                (!consideredSet.has(`${neighborRow},${neighborCol}`)) &&
                graph[neighborRow][neighborCol] !== 0
            ) {
                queue.push(neighbor);
                consideredSet.add(`${neighborRow},${neighborCol}`)
            }
        }
    }

    return islandSize;
};


const i4 = [
    [1, 1, 1, 0, 1, 0],
    [0, 0, 0, 0, 1, 1],
    [0, 0, 0, 0, 1, 0],
    [1, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1],
    [0, 1, 1, 1, 0, 1],
]
console.log(minIslandSize(i4));
// 2

const i1 = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
]
console.log(minIslandSize(i1));
// 0

const i2 = [
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
]
console.log(minIslandSize(i2));
// 36

const i3 = [
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0, 1],
]
console.log(minIslandSize(i3));
// 1

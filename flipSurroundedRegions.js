"use strict"

const flipSurroundedRegions = (board) => {
    for (let r = 0; r < board.length; r++) {
        for (let c = 0; c < board[0].length; c++) {
            let considered = new Set();
            const fullySurrounded = explore(board, r, c, considered);

            if (fullySurrounded) {
                for (const node of considered) {
                    const [row, col] = node.split(",");
                    board[row][col] = "X";
                }
            }
        }
    }
};


const explore = (board, r, c, considered) => {
    if (board[r][c] === "X") return;

    considered.add(`${r},${c}`);
    let fullySurrounded = true;

    const neighbors = [
        [r - 1, c],
        [r + 1, c],
        [r, c - 1],
        [r, c + 1]
    ]

    for (const neighbor of neighbors) {
        const [neighborRow, neighborCol] = neighbor;
        let neighborSurrounded = true;

        if (
            neighborRow < 0 ||
            neighborRow >= board.length ||
            neighborCol < 0 ||
            neighborCol >= board[0].length
        ) {
            fullySurrounded = false;
        }
        else if (
            board[neighborRow][neighborCol] !== "X" &&
            !considered.has(`${neighborRow},${neighborCol}`)
        ) {
            neighborSurrounded = explore(board, neighborRow, neighborCol, considered);
        }

        fullySurrounded = fullySurrounded && neighborSurrounded;
    }

    return fullySurrounded;
}


let grid;

grid = [
    ["O","X","X","O","X"],
    ["X","O","O","X","O"],
    ["X","O","X","O","X"],
    ["O","X","O","O","O"],
    ["X","X","O","X","O"]
];
console.log(flipSurroundedRegions(grid));
/*
[
    ["O","X","X","O","X"],
    ["X","X","X","X","O"],
    ["X","X","X","O","X"],
    ["O","X","O","O","O"],
    ["X","X","O","X","O"]
]
*/

grid = [
    ["X","X","X","X"],
    ["X","O","O","X"],
    ["X","X","O","X"],
    ["X","O","X","X"]
];
console.log(flipSurroundedRegions(grid));
/*
[
    ["X","X","X","X"],
    ["X","X","X","X"],
    ["X","X","X","X"],
    ["X","O","X","X"]
]
*/

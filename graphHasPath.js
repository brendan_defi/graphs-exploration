"use strict"

const hasPathIterativeDFS = (graph, source, target) => {
    const stack = [source];

    while (stack.length > 0) {
        const currentNode = stack.pop();
        console.log(currentNode);
        if (currentNode === target) return true;

        const neighbors = graph[currentNode];
        for (const neighbor of neighbors) {
            stack.push(neighbor);
        }
    }
    return false;
};


const hasPathRecursiveDFS = (graph, source, target) => {
    if (source === target) return true;

    const neighbors = graph[source];
    while (neighbors.length > 0) {
        const currentNode = neighbors.pop();
        const hasPath = hasPathRecursiveDFS(graph, currentNode, target);
        if (hasPath) return true;
    }
    return false;
}


const hasPathBFS = (graph, source, target) => {
    const queue = [source];

    while (queue.length > 0) {
        const currentNode = queue.shift();
        if (currentNode === target) return true;

        const neighbors = graph[currentNode];
        for (const neighbor of neighbors) {
            queue.push(neighbor);
        }
    }
    return false;
}

let g = {
    f: ["g", "i"],
    g: ["h"],
    h: [],
    i: ["g", "k"],
    j: ["i"],
    k: []
}
// console.log(hasPathIterativeDFS(g, "f", "k"))
// console.log(hasPathRecursiveDFS(g, "f", "k"))
console.log(hasPathBFS(g, "f", "k"))
// should be true
// console.log(hasPathIterativeDFS(g, "j", "f"))
// console.log(hasPathRecursiveDFS(g, "j", "f"))
console.log(hasPathBFS(g, "j", "f"))
// should be false

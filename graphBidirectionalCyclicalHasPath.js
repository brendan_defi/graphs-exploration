"use strict"

const hasPathUndirectedGraph = (edges, source, target) => {
    const graph = edgeListToAdjacencyMap(edges);
    // return hasPathRecursiveDFS(graph, source, target)
    return hasPathBFS(graph, source, target)
};


const hasPathRecursiveDFS = (graph, src, dst, seen = new Set()) => {
    if (src === dst) return true;
    seen.add(src);

    for (const neighbor of graph[src]) {
        if (!seen.has(neighbor)) {
            const hasPath = hasPathRecursiveDFS(graph, neighbor, dst, seen);
            if (hasPath) return true;
        }
    }
    return false;
};


const hasPathBFS = (graph, src, dst, seen = new Set()) => {
    const queue = [src];

    while (queue.length > 0) {
        const currentNode = queue.shift();
        if (currentNode === dst) return true;
        if (seen.has(currentNode)) {
            continue
        }
        else {
            seen.add(currentNode);
        }

        for (const neighbor of graph[currentNode]) {
            if (!seen.has(neighbor)) {
                queue.push(neighbor)
            }
        }
    }
    return false;
};


const edgeListToAdjacencyMap = (edges) => {
    const adjacencyMap = {};

    for (const edge of edges) {
        const [node1, node2] = edge;

        if (adjacencyMap[node1]) {
            adjacencyMap[node1].push(node2);
        }
        else {
            adjacencyMap[node1] = [node2]
        }

        if (adjacencyMap[node2]) {
            adjacencyMap[node2].push(node1);
        }
        else {
            adjacencyMap[node2] = [node1]
        }
    }
    return adjacencyMap;
}


const e = [
    ["i", "j"],
    ["k", "i"],
    ["m", "k"],
    ["k", "l"],
    ["o", "n"]
]

console.log(hasPathUndirectedGraph(e, "j", "m"))
// true

console.log(hasPathUndirectedGraph(e, "i", "o"))
// false

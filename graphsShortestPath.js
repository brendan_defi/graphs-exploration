"use strict"

const shortestPath = (edges, src, dst) => {
    const graph = edgelistToAdjacencyMap(edges);
    const seen = new Set();
    const queue = [[src, 0]];

    while (queue.length > 0) {
        const [currentNode, currentDistance] = queue.shift();
        seen.add(currentNode);
        if (currentNode === dst) {
            return currentDistance;
        }

        for (const neighbor of graph[currentNode]) {
            if (!seen.has(neighbor)) {
                queue.push([neighbor, currentDistance + 1]);
            }
        }
    }

    return -1;
};


const edgelistToAdjacencyMap = (edges) => {
    const adjacencyMap = {}

    for (const [node1, node2] of edges) {
        if (!(node1 in adjacencyMap)) adjacencyMap[node1] = [];
        if (!(node2 in adjacencyMap)) adjacencyMap[node2] = [];
        adjacencyMap[node1].push(node2)
        adjacencyMap[node2].push(node1)
    }

    return adjacencyMap
};




const e = [
    ["w", "x"],
    ["x", "y"],
    ["z", "y"],
    ["z", "v"],
    ["w", "v"],
    ["a", "b"]
]


console.log(shortestPath(e, "w", "a"));

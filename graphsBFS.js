"use strict"


const iterativeBreadthFirstSearch = (graph, startingNode) => {
    const queue = [startingNode];

    while (queue.length > 0) {
        const currentNode = queue.shift();
        console.log(currentNode);

        const neighbors = graph[currentNode];
        for (const neighbor of neighbors) {
            queue.push(neighbor)
        }
    }
};

const g = {
    a: ['b', 'c'],
    b: ['d'],
    c: ['e'],
    d: ['f'],
    e: [],
    f: [],
}

console.log(iterativeBreadthFirstSearch(g, "a"));

"use strict"

const rotSpread = (grid) => {
    let days = 0;
    let healthyCount = getHealthyCount(grid);
    let considered = new Set();

    let rotIncreased = true;
    while (rotIncreased) {
        rotIncreased = false;
        const newRottenCells = [];
        for (let row = 0; row < grid.length; row++) {
            for (let col = 0; col < grid[0].length; col++) {
                if (
                    grid[row][col] === 2 &&
                    !(considered.has(`${row},${col}`))
                ) {
                    considered.add(`${row},${col}`)
                    const neighbors = [
                        [row - 1, col],
                        [row + 1, col],
                        [row, col - 1],
                        [row, col + 1],
                    ];

                    for (const neighbor of neighbors) {
                        const [neighborRow, neighborCol] = neighbor;
                        if (
                            neighborRow >= 0 &&
                            neighborRow < grid.length &&
                            neighborCol >= 0 &&
                            neighborCol < grid[0].length &&
                            grid[neighborRow][neighborCol] === 1
                        ) {
                            newRottenCells.push(neighbor);
                            rotIncreased = true;
                        };
                    };
                };
            };
        };

        if (rotIncreased) {
            for (const newRottenCell of newRottenCells) {
                const [cellRow, cellCol] = newRottenCell;
                if (grid[cellRow][cellCol] === 1) {
                    grid[cellRow][cellCol] = 2
                    healthyCount--;
                };
            };
            days++;
            if (healthyCount === 0) return days;
        };
    };

    if (healthyCount !== 0) {
        return -1
    };

    return days;
};


const getHealthyCount = (grid) => {
    let healthyCount = 0;
    for (let row = 0; row < grid.length; row++) {
        for (let col = 0; col < grid[0].length; col++) {
            if (grid[row][col] === 1) healthyCount++;
        };
    };

    return healthyCount;
};



let g;

g = [
    [2,1,1],
    [0,1,1],
    [1,0,1]
];
console.log(rotSpread(g));
// -1

g = [
    [2,1,1],
    [1,1,0],
    [0,1,1]
];
console.log(rotSpread(g));
// 4

g = [[0,2]];
console.log(rotSpread(g));
// 0

g = [
    [2,1,1,1],
    [1,0,1,1],
    [1,0,1,2]
];
console.log(rotSpread(g));
// 2

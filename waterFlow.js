"use strict"

const waterFlow = (heights) => {
    const cellsThatFlowIntoBothOceans = [];

    for (let row = 0; row < heights.length; row++) {
        for (let col = 0; col < heights[0].length; col++) {
            const root = [row, col];
            let considered = new Set();
            const [currentFlowsToPacific, currentFlowsToAtlantic] = explore(heights, root, considered);

            if (currentFlowsToPacific && currentFlowsToAtlantic) cellsThatFlowIntoBothOceans.push([row, col]);
        }
    }

    return cellsThatFlowIntoBothOceans;
};


const explore = (grid, src, considered) => {
    const [startingRow, startingCol] = src;
    considered.add(`${startingRow},${startingCol}`)


    let srcFlowsToPacific = false;
    let srcFlowsToAtlantic = false;


    const neighbors = [
        [startingRow - 1, startingCol],
        [startingRow + 1, startingCol],
        [startingRow, startingCol - 1],
        [startingRow, startingCol + 1],
    ]

    for (const neighbor of neighbors) {
        const [neighborRow, neighborCol] = neighbor;

        // first check if the "neighbor" is actually one of the oceans
        if (
            neighborRow < 0 ||
            neighborCol < 0
        ) srcFlowsToPacific = true;

        if (
            neighborRow >= grid.length ||
            neighborCol >= grid[0].length
        ) srcFlowsToAtlantic = true;

        // then see if we can pursue the neighbor's path
        if (
            // we check if the neighbor is still on the island...
            neighborRow >= 0 &&
            neighborRow < grid.length &&
            neighborCol >= 0 &&
            neighborCol < grid[0].length &&
            // ... whether we're allowed to get there...
            grid[startingRow][startingCol] >= grid[neighborRow][neighborCol] &&
            // ... and whether we've been there before
            !considered.has(`${neighborRow},${neighborCol}`)
        ) {
            const [neighborReachesPacific, neighborReachesAtlantic] = explore(grid, neighbor, considered)

            // then we reevaluate whether the root can reach both oceans
            if (neighborReachesPacific) srcFlowsToPacific = true;
            if (neighborReachesAtlantic) srcFlowsToAtlantic = true;
        }

    }

    return [srcFlowsToPacific, srcFlowsToAtlantic]
};





let x;

x = [
    [1,2,2,3,5],
    [3,2,3,4,4],
    [2,4,5,3,1],
    [6,7,1,4,5],
    [5,1,1,2,4]
];
console.log(waterFlow(x));
// [[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]

x = [[1]];
console.log(waterFlow(x));
// [[0,0]]
